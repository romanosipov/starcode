<?php
	include 'src/services.php';
	
	$card = find(intval($_GET['card_id']));
?>

<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>Задание 2. PHP - История покупок</title>

    <!-- Bootstrap core CSS -->
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/task2.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">Задания Starcode</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.html">В начало</a></li>
            <li><a href="task1.html">Задание 1. JavaScript</a></li>
            <li class="active"><a href="task2.php">Задание 2. PHP</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Задание 2. PHP - История покупок по карте <?php echo sprintf('%03d', $card->getSerie()); ?>, <?php echo sprintf('%06d', $card->getNom()); ?></h1>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
      	<h1>История покупок по карте пуста</h1>
      	
      </div>
      
      
      <div class="panel panel-default">
      	<div clas="panel-body">
      		<h3>Профиль карты</h3>
      		
      		<p><strong>Серия, номер</strong>: <?php echo sprintf('%03d', $card->getSerie()); ?>, <?php echo sprintf('%06d', $card->getNom()); ?></p>
      		<p><strong>Дата выпуска</strong>: <?php echo $card->getIssueDate()->format("d.m.Y H:i"); ?></p>
      		<p><strong>Дата окончания активности</strong>: <?php echo $card->getExpiredDate()->format("d.m.Y H:i"); ?></p>
      		<p>...</p>
      	</div>
      </div>

      <hr>

      <footer>
        <p>&copy; Роман Осипов 2014</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
  </body>
</html>

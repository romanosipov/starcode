<?php

include "src/services.php";

$serie = intval($_POST['serie']);
$quantity = intval($_POST['quantity']);
$expired_period = intval($_POST['expired_period']);

generate_cards($serie, $quantity, $expired_period);

header("Location: task2.php");

<?php

include "src/services.php";

$params = array();

if (!empty($_POST['serie'])) {
	$params['serie'] = intval($_POST['serie']);
}

if (!empty($_POST['nom'])) {
	$params['nom'] = intval($_POST['nom']);
}

if (intval($_POST['status']) != 0) {
	$params['status'] = intval($_POST['status']);
}

if (!empty($_POST['periodBegin'])) {
	$params['periodBegin'] = DateTime::createFromFormat('d.m.Y', $_POST['periodBegin']);
	$params['periodBegin']->setTime(0,0,0);
}

if (!empty($_POST['periodEnd'])) {
	$params['periodEnd'] = DateTime::createFromFormat('d.m.Y', $_POST['periodEnd']);
	$params['periodEnd']->setTime(23,59,59);
}

$cards = search_cards($params);

// echo $params['periodBegin']->format('Y-m-d H:i:s');

include "task2.php"; 
/** Приложение. */
var app = angular.module('task1App', []);

/** Контроллер списка сотрудников. */
app.controller("EmpCtrl", function($scope) {
	// Список сотрудников
	$scope.emps = [
		{checked: false, name: "Иванов Иван"},
		{checked: false, name: "Петров Петр"},
		{checked: false, name: "Зеленый Вениамин"},
		{checked: false, name: "Гербулов Иван"},
		{checked: false, name: "Осипова Анна"},
		{checked: false, name: "Мелкумянц Алексей"},
		{checked: true, name: "Черный Алексей"},
		{checked: true, name: "Король Иван"},
		{checked: true, name: "Сидоров Денис"}
	];
	
	$scope.onClick = function(emp) {
		emp.checked = !emp.checked;
	};
	
	/** Начальная сортировка массива сотрудников. */
	function compare(a,b) {
	  if (a.name < b.name)
	     return -1;
	  if (a.name > b.name)
	    return 1;
	  return 0;
	};
	
	$scope.emps.sort(compare);
});

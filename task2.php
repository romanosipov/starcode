<?php
	include_once 'src/services.php';
?>

<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>Задание 2. PHP</title>

    <!-- Bootstrap core CSS -->
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/task2.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">Задания Starcode</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.html">В начало</a></li>
            <li><a href="task1.html">Задание 1. JavaScript</a></li>
            <li class="active"><a href="task2.php">Задание 2. PHP</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Задание 2. PHP</h1>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
      	<h1>Бонусные карты</h1>
      	
      	<div class="btn-group">
      		<button type="button" class="btn btn-default" data-toggle="modal" data-target="#genCardsModal" title="Генерация карт"><span class="glyphicon glyphicon-plus"</button>
<!--      		<button type="button" class="btn btn-default" title="Удалить карты"><span class="glyphicon glyphicon-minus"</button>-->
      		<button type="button" class="btn btn-default" data-toggle="modal" data-target="#searchModal" title="Поиск карт"><span class="glyphicon glyphicon-search"</button>
<!--      		<button type="button" class="btn btn-default" title="Генерация карт"><span class="glyphicon glyphicon-list"</button>-->
      	</div>
      	
      	<table class="table table-striped table-bordered table-hover table-condensed" style="margin-top: 1em;">
      		<thead>
      			<th class="tac"><input type="checkbox" /></th>
      			<th class="tac">Серия, номер</th>
      			<th class="tac">Дата выпуска<br />Дата окончания активности</th> 
      			<th class="tac">Статус</th>
      			<th class="tac">Действия</th>
      		</thead>
			<?php
				if (!isset($cards)) {
					$cards = find_all();
				}
			?>
      		<tbody>
      		<?php 
      			foreach ($cards as &$card) { 
      		?>
      		<tr>	
      			<td class="tac"><input type="checkbox" /></td>
      			<td class="tac"><a href="task2_history.php?card_id=<?php echo $card->getId() ;?>"><strong><?php echo sprintf('%03d', $card->getSerie()); ?>, <?php echo sprintf('%06d', $card->getNom()); ?></strong></a></td>
      			<td class="tac"><?php echo $card->getIssueDate()->format("d.m.Y H:i"); ?><br/><?php echo $card->getExpiredDate()->format("d.m.Y H:i"); ?></td>
      			<td class="tac">
      				<?php
      				switch ($card->getStatus()) {
						case 1:
							echo "не активирована";
							break;
						case 2:
							echo "активирована";
							break;
						default:
							echo "просрочена";
      				}
      				?>
      			</td>
      			<td class="tac">
      				<div class='btn-group'>
      					<a href="task2_history.php?card_id=<?php echo $card->getId() ;?>"><button type="button" class="btn btn-default" title="История покупок"><span class="glyphicon glyphicon-tasks"></span></button></a>
      					<a href="task2_remove.php?card_id=<?php echo $card->getId() ;?>"><button type="button" class="btn btn-default" title="Удалить карту"><span class="glyphicon glyphicon-remove"></span></button></a>
      					<?php
      					if ($card->getStatus() == 1) {
      					?>
      					<a href="task2_activate.php?card_id=<?php echo $card->getId() ;?>"><button type="button" class="btn btn-default" title="Активировать карту"><span class="glyphicon glyphicon-play"></span></button></a> 
      					<?php
      					} else if ($card->getStatus() == 2) {
      					?>
      					<a href="task2_deactivate.php?card_id=<?php echo $card->getId() ;?>"><button type="button" class="btn btn-default" title="Деактивировать карту"><span class="glyphicon glyphicon-ban-circle"></span></button></a>
      					<?php
      					}
						?>
      				</div>
      			</td>
      		</tr>
      		<?php
				} 
			?>
      		</tbody>
      	</table>
      	
      </div>

      <hr>

      <footer>
        <p>&copy; Роман Осипов 2014</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="js/moment-with-langs.min.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <script src="js/task2.js"></script>
    
    <div class="modal fade" id="genCardsModal" tabindex="-1" role="dialog" aria-labelledby="genCardsModalLabel" aria-hidden="true">
    	<form class="form-horizontal" role="form" action="task2_gen_cards.php" method="POST" enctype="multipart/form-data">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    				<h4 class="modal-title" id="genCardsModalLabel">Генерация бонусных карт</h4>
    			</div>
    			
    			<div class="modal-body">
    				<div class="row">
    					<div class="col-md-6">
    						<div class="form-group">
    							<label for="serie" class="col-sm-4 control-label">Серия</label>
    							<div class="col-sm-8">
    								<input type="text" class="form-control" name="serie" id="serie" placeholder="Серия..." />
    							</div>
    						</div>
    					</div>
    					
    					<div class="col-md-6">
    						<div class="form-group">
    							<label for="quantity" class="col-sm-4 control-label">Кол-во</label>
    							<div class="col-sm-8">
    								<input type="text" class="form-control" name="quantity" id="quantity" placeholder="Количество карт..." />
    							</div>
    						</div>
    					</div>
    				</div>
    				
    				<div class="form-group">
    					<label for="expiredPeriod" class="col-sm-6 control-label">Срок окончания активности</label>
    					<select name="expired_period" id="expiredPeriod" class="col-sm-6">
    						<option value="1">1 год</option>
    						<option value="2">6 месяцев</option>
    						<option value="3">1 месяц</option>
    					</select>
    				</div>
    			</div>
    			
    			<div class="modal-footer">
    				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
    				<button type="submit" class="btn btn-primary">Выполнить генерацию</button>
    			</div>
    		</div>
    	</div>
    	</form>
    </div>

    <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalLabel" aria-hidden="true">
    	<form class="form-horizontal" role="form" action="task2_search.php" method="POST" enctype="multipart/form-data">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    				<h4 class="modal-title" id="genCardsModalLabel">Генерация бонусных карт</h4>
    			</div>
    			
    			<div class="modal-body">
    				<div class="row">
    					<div class="col-md-6">
    						<div class="form-group">
    							<label for="smNom" class="col-sm-4 control-label">Серия</label>
    							<div class="col-sm-8">
    								<input type="text" class="form-control" name="serie" id="smSerie" placeholder="Серия..." />
    							</div>
    						</div>
    					</div>
    					
    					<div class="col-md-6">
    						<div class="form-group">
    							<label for="smNom" class="col-sm-4 control-label">Номер</label>
    							<div class="col-sm-8">
    								<input type="text" class="form-control" name="nom" id="smNom" placeholder="Номер..." />
    							</div>
    						</div>
    					</div>
    				</div>
    				
    				<div class="form-group">
    					<label for="smStatus" class="col-sm-4 control-label">Статус карты</label>
    					<select name="status" id="smStatus" class="col-sm-8">
    						<option value="0">Не имеет значения</option>
    						<option value="1">Не активирована</option>
    						<option value="2">Активирована</option>
    						<option value="3">Просрочена</option>
    					</select>
    				</div>
    		
    				<p>Дата выпуска карты (с - по)</p>
    			
    				<div class="row">
   						<div class="col-md-6">
                			<div class='input-group date' id='datetimepicker1'>
                    			<input type='text' name="periodBegin" class="form-control" data-format="DD.MM.YYYY" />
                    			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                			</div>
    					</div>

   						<div class="col-md-6">
                			<div class='input-group date' name="periodEnd" id='datetimepicker1'>
                    			<input type='text' name="periodEnd" class="form-control" data-format="DD.MM.YYYY" />
                    			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                			</div>
    					</div>
    				</div>
    			</div>
    			
    			
    			<div class="modal-footer">
    				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
    				<button type="submit" class="btn btn-primary">Найти</button>
    			</div>
    		</div>
    	</div>
    	</form>
    </div>
  </body>
</html>

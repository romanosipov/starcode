<?php
include 'entities.php';
include 'vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$paths = array(__DIR__);
$isDevMode = true;

$dbParams = array(
	'driver' => 'pdo_mysql',
	'user' => 'starcode',
	'password' => 'password',
	'dbname' => 'starcode'
);

$config = Setup::createAnnotationMEtadataConfiguration($paths, $isDevMode);

$em = EntityManager::create($dbParams, $config);

/** Отбирает все бонóсные карты. */
function find_all() {
	global $em;
	
	$qb = $em->createQueryBuilder();
	
	return $qb->select('bc')->from('Entities\BonusCard', 'bc')->orderBy("bc.serie, bc.nom", "ASC")->getQuery()->getResult();
};

/** Выполняет поиск карт по óказанным критериям. */
function search_cards($params) {
	global $em;
	
	$qb = $em->createQueryBuilder();
	
	$qb = $qb->select('bc')->from('Entities\BonusCard', 'bc');
	
	if (isset($params['serie'])) {
		$qb = $qb->where($qb->expr()->eq("bc.serie", ":serie"))->setParameter("serie", $params['serie']);
	}

	if (isset($params['nom'])) {
		$qb = $qb->where($qb->expr()->eq("bc.nom", ":nom"))->setParameter("nom", $params['nom']);
	}

	if (isset($params['status'])) {
		$qb = $qb->where($qb->expr()->eq("bc.status", ":status"))->setParameter("status", $params['status']);
	}

	if (isset($params['periodBegin'])) {
		$qb = $qb->where($qb->expr()->gte("bc.issueDate", ":issueDate"))->setParameter("issueDate", $params['periodBegin']);
	}
	
	if (isset($params['periodEnd'])) {
		$qb = $qb->where($qb->expr()->lte("bc.issueDate", ":issueDateEnd"))->setParameter("issueDateEnd", $params['periodEnd']);
	}

	$qb = $qb->orderBy("bc.serie, bc.nom", "ASC");
	
	return $qb->getQuery()->getResult();
};

/** Находит картó по идентификаторó. */
function find($card_id) {
	global $em;

	return $em->getRepository("Entities\BonusCard")->findOneById($card_id);
};

/** Выполняет генерацию карты. */
function generate_cards($serie, $quantity, $expired_period) {
	global $em;
	
	// Начальная дата
	$issue_date = new DateTime();
	
	// Дата окончания активности
	$period_length = 0;
	if ($expired_period == 1) {
		$period_length = new DateInterval("P1Y");
	} else if ($expired_period == 2) {
		$period_length = new DateInterval("P6M");
	} else if ($expired_period == 3) {
		$period_length = new DateInterval("P1M");
	}
	
	$expired_date = clone $issue_date;
	$expired_date = $expired_date->add($period_length);
	
	// Последний номер в серии
	$first_nom = 1;
	
	$qb = $em->createQueryBuilder();
	$qb->select("bc")->from("Entities\BonusCard", "bc")->where($qb->expr()->eq("bc.serie", "?1"))->setParameter(1, $serie)->orderBy("bc.nom", "DESC")->setMaxResults(1);
	$last_card = $qb->getQuery()->getOneOrNullResult();
	
	if ($last_card != null) {
		$first_nom = $last_card->getNom() + 1;
	}
	
	for ($n = $first_nom; $n < ($quantity + $first_nom); $n++) {
		$card = new Entities\BonusCard();
		$card->setSerie($serie);
		$card->setNom($n);
		$card->setIssueDate($issue_date);
		$card->setExpiredDate($expired_date);
		$card->setStatus(1);
		
		$em->persist($card);
		$em->flush();
	}
};

/** Активирóет картó. */
function activate_card($card_id) {
	global $em;
	
	$card = $em->getRepository("Entities\BonusCard")->findOneById($card_id);
	$card->setStatus(2);
	$em->flush();
};

/** Деактивирóет картó. */
function deactivate_card($card_id) {
	global $em;
	
	$card = $em->getRepository("Entities\BonusCard")->findOneById($card_id);
	$card->setStatus(1);
	$em->flush();
};

/** Óдаляет картó. */
function remove_card($card_id) {
	global $em;
	
	$card = $em->getRepository("Entities\BonusCard")->findOneById($card_id);
	$em->remove($card);
	$em->flush();
};

date_default_timezone_set("Europe/Moscow");
<?php
namespace Entities;

/**
 * @Entity
 * @Table(name="bonus_card")
 */
class BonusCard {
	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 */
	private $id;
	
	/** @Column(type="integer", name="serie", nullable=false) */
	private $serie;
	
	/** @Column(type="integer", name="nom", nullable=false) */
	private $nom;
	
	/** @Column(type="datetime", name="issue_date", nullable=false) */
	private $issueDate;
	
	/** @Column(type="datetime", name="expired_date", nullable=false) */
	private $expiredDate;
	
	/** @Column(type="datetime", name="using_date", nullable=true) */
	private $usingDate;
	
	/** @Column(type="integer", name="status", nullable=false) */
	private $status;
	
	public function getId() {
		return $this->id;
	}
	
	public function setId($id) {
		$this->id = $id;
	}

	public function getSerie() {
		return $this->serie;
	}
	
	public function setSerie($serie) {
		$this->serie = $serie;
	}

	public function getNom() {
		return $this->nom;
	}
	
	public function setNom($nom) {
		$this->nom = $nom;
	}

	public function getIssueDate() {
		return $this->issueDate;
	}
	
	public function setIssueDate($issueDate) {
		$this->issueDate = $issueDate;
	}

	public function getExpiredDate() {
		return $this->expiredDate;
	}
	
	public function setExpiredDate($expiredDate) {
		$this->expiredDate = $expiredDate;
	}

	public function getUsingDate() {
		return $this->usingDate;
	}
	
	public function setUsingDate($usingDate) {
		$this->usingDate = $usingDate;
	}

	public function getStatus() {
		return $this->status;
	}
	
	public function setStatus($status) {
		$this->status = $status;
	}
}
